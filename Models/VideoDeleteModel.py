from PyQt5.QtWidgets import QDialog
from GUIs.VideoDeleteDialog import Ui_VideoRemove
from Database.Database import VideoDatabase
from Misc.functions import delete_file
from Misc.ConfigFunctions import project_dir_read
from pathlib import Path


class DeleteVideo(QDialog):
    def __init__(self, video):
        super(DeleteVideo, self).__init__()
        self.ui = Ui_VideoRemove()
        self.ui.setupUi(self)
        self.db = VideoDatabase()
        self.video = video
        self.project_dir = project_dir_read()

        self.ui.btn_yes.clicked.connect(self.yes_clicked)
        self.ui.btn_no.clicked.connect(self.no_clicked)

    def yes_clicked(self):
        self.db.delete_video(self.video)
        if self.ui.check_rm_project.isChecked():
            video_path = self.video.path
            if Path(self.project_dir) in Path(video_path).parents:
                print("VideoDeleteModel: delete video from project dir")
                delete_file(video_path)
        self.close()

    def no_clicked(self):
        self.close()
