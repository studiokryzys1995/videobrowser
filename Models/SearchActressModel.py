from GUIs.SearchActress import Ui_Dialog
from PyQt5.QtWidgets import QDialog
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, QStringListModel
from Models.AddActressModel import AddActress
from Database.Database import VideoDatabase


class SearchActress(QDialog):
    def __init__(self):
        super(SearchActress, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.db = VideoDatabase()
        self.ui.btn_search.clicked.connect(self.search)
        self.ui.list_search.clicked.connect(self.item_clicked)
        self.ui.btn_select.clicked.connect(self.accept_chose)
        self.ui.btn_add.clicked.connect(self.add_actress)
        self.ui.btn_del.clicked.connect(self.del_actress)
        self.search_result = None
        self.selected = []
        self.search()

    def search(self):
        if self.ui.edit_search.text():
            self.search_result = self.db.select_actress_by_name(self.ui.edit_search.text())
        else:
            self.search_result = self.db.select_all_actress()
        model = QStringListModel(self)
        names = []
        for act in self.search_result:
            names.append(act.name)
        names.sort()
        model.setStringList(names)
        self.ui.list_search.setModel(model)

    def item_clicked(self, index):
        for act in self.search_result:
            if act.name == index.data():
                self.ui.label_avatar.setPixmap(QPixmap(act.avatar).scaled(
                                            self.ui.label_avatar.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation))

    def add_actress(self):
        a = AddActress(self.db)
        a.exec()

    def del_actress(self):
        pass

    def accept_chose(self):
        indexes = self.ui.list_search.selectedIndexes()
        for act in self.search_result:
            for sel in indexes:
                if act.name == sel.data():
                    self.selected.append(act)
                    indexes.remove(sel)
                    break
        self.close()
