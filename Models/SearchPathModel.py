from GUIs.SearchFolders import Ui_Dialog
from PyQt5.QtWidgets import QDialog, QFileDialog
from PyQt5.QtCore import QStringListModel
from Misc import functions


class SearchPath(QDialog):
    def __init__(self):
        super(SearchPath, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.btn_open.clicked.connect(self.search)

    def search(self):
        dialog = QFileDialog()
        path = dialog.getExistingDirectory(self, "Chose folder", "M:/")
        self.ui.label_path.setText(path)
        files = functions.find_videos_in_path(path, recursive=True)
        print(files)
        model = QStringListModel(self)
        model.setStringList(files)
        self.ui.list_files.setModel(model)

