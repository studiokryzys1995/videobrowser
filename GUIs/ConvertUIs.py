import os
import glob

for f in glob.glob("*.ui"):
    string = r"P:\ProgramingProjects\PythonProjects\venv\Scripts\pyuic5.exe -x " + os.path.abspath(f) \
             + " -o " + os.path.splitext(os.path.abspath(f))[0] + ".py"
    os.system(string)
