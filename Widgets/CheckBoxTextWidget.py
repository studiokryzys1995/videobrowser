from PyQt5.QtWidgets import QLabel, QCheckBox, QHBoxLayout, QFrame, QSizePolicy, QWidget
from PyQt5.QtCore import Qt


class CheckBoxTextWidget(QFrame):
    def __init__(self, text):
        super().__init__()

        self.label = QLabel(text)
        self.label.setAlignment(Qt.AlignLeft)
        self.check_box = QCheckBox()
        self.check_box.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Maximum))
        # self.label.setFrameShape(QFrame.Box)
        # self.setFrameShape(QFrame.Box)

        self.lay = QHBoxLayout()
        self.lay.addWidget(self.check_box)
        self.lay.addWidget(self.label)
        self.lay.setSpacing(10)
        self.lay.setContentsMargins(0, 0, 0, 0)

        self.setLayout(self.lay)

        self.setSizePolicy(QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed))

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.check_box.setChecked(not self.check_box.isChecked())
            event.accept()

    def is_checked(self):
        return self.check_box.isChecked()

    def get_data(self):
        return self
