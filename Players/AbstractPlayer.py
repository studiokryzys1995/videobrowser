from PyQt5.QtCore import QUrl, Qt
from PyQt5.QtWidgets import QSlider, QDialog, QVBoxLayout, QHBoxLayout, QPushButton, QStyle, QSpacerItem, \
                            QSizePolicy, QLabel, QFrame
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from Widgets.MovableSlider import MovableSlider
from Misc import functions


class Player(QDialog):
    def __init__(self):
        super(Player, self).__init__()
        self.setWindowFlags(self.windowFlags() | Qt.WindowMinimizeButtonHint | Qt.WindowMaximizeButtonHint)
        self.setObjectName("Player")
        self.resize(1024, 800)
        self.setWindowTitle("Player")
        self.video_surface = None
        self.player = QMediaPlayer(None, QMediaPlayer.VideoSurface)

        self.player.stateChanged.connect(self.mediaStateChanged)
        self.player.mutedChanged.connect(self.muteStateChanged)
        self.player.positionChanged.connect(self.positionChanged)
        self.player.durationChanged.connect(self.durationChanged)
        self.player.volumeChanged.connect(self.volumeChanged)

        self.positionSlider = MovableSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderMoved.connect(self.setPosition)

        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)

        self.position_label = QLabel("00:00:00")
        self.position_label.setMargin(0)
        self.position_label.setFrameShape(QFrame.NoFrame)
        self.duration_label = QLabel("/00:00:00")
        self.duration_label.setFrameShape(QFrame.NoFrame)
        self.duration_label.setMargin(0)

        self.muteButton = QPushButton()
        self.muteButton.setEnabled(False)
        self.muteButton.setIcon(self.style().standardIcon(QStyle.SP_MediaVolume))
        self.muteButton.clicked.connect(self.mute)

        self.volumeSlider = QSlider(Qt.Horizontal)
        self.volumeSlider.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Maximum)
        self.volumeSlider.setRange(0, 100)
        self.volumeSlider.sliderMoved.connect(self.setVolume)
        self.volumeSlider.setValue(self.player.volume())

        self.spacer = QSpacerItem(50, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.layout = QVBoxLayout()

    def init_layout(self):
        controlLayout = QVBoxLayout()
        #controlLayout.setSpacing(0)
        controlLayout.setContentsMargins(0, 0, 0, 0)
        controlLayout.addWidget(self.positionSlider)
        controlsLayout = QHBoxLayout()
        controlsLayout.addWidget(self.playButton)
        controlsLayout.addItem(self.spacer)
        controlsLayout.addWidget(self.position_label)
        controlsLayout.addWidget(self.duration_label)
        controlsLayout.addWidget(self.muteButton)
        controlsLayout.addWidget(self.volumeSlider)
        controlLayout.addLayout(controlsLayout)
        self.layout.addLayout(controlLayout)
        self.setLayout(self.layout)

    def set_video_surface(self, video_surface):
        self.video_surface = video_surface

    def showEvent(self, event):
        super().showEvent(event)
        self.play()
        self.play()

    def set_video(self, url):
        url = QUrl.fromLocalFile(url)
        try:
            self.player.setMedia(QMediaContent(url))
            self.playButton.setEnabled(True)
            self.muteButton.setEnabled(True)
        except Exception as e:
            print(e)

    def play(self):
        if self.player.state() == QMediaPlayer.PlayingState:
            self.player.pause()
        else:
            if self.player.state() == 0:
                self.player.setVideoOutput(self.video_surface)
            self.player.play()

    def mute(self):
        if self.player.isMuted():
            self.player.setMuted(False)
        else:
            self.player.setMuted(True)

    def mediaStateChanged(self, state):
        if self.player.state() == QMediaPlayer.PlayingState:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPlay))

    def muteStateChanged(self, state):
        if self.player.isMuted():
            self.muteButton.setIcon(
                self.style().standardIcon(QStyle.SP_MediaVolumeMuted))
        else:
            self.muteButton.setIcon(
                self.style().standardIcon(QStyle.SP_MediaVolume))

    def setPosition(self, position):
        self.player.setPosition(position)

    def positionChanged(self, position):
        self.positionSlider.setValue(position)
        self.position_label.setText(functions.duration_to_sting(position))

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)
        self.duration_label.setText("/" + functions.duration_to_sting(duration))

    def setVolume(self, volume):
        self.player.setVolume(volume)

    def volumeChanged(self, volume):
        self.volumeSlider.setValue(volume)
