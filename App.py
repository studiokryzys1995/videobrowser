from PyQt5 import QtWidgets
from GUIs.Browser import Ui_MainWindow
from Widgets.SmallWidget import SmallWidget
from Database.Database import VideoDatabase
from Models.AddVideoModel import AddVideo
from Models.AddActressModel import AddActress
from Models.ViewActressModel import ViewActress
from Models.AddTabModel import AddTab
from Models.SearchPathModel import SearchPath
from Models.BatchMoveModel import BatchMoveModel
from Misc import ConfigFunctions, functions
from PyQt5.QtWidgets import QAction, QMessageBox
from PyQt5.QtCore import Qt
import sys

about_text = """
Vidbro v0.8
Author: Konrad Bober
Email: konradbober1995@gmail.com
"""


def about():
    msg_box = QMessageBox()
    msg_box.setWindowTitle("About")
    msg_box.setText(about_text)
    msg_box.exec()


class MainApp(QtWidgets.QMainWindow):

    def __init__(self):
        super(MainApp, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.anotherLayout = QtWidgets.QVBoxLayout(self.ui.scrollAreaWidgetContents)
        self.grid_container = QtWidgets.QWidget()
        self.gridLayout = QtWidgets.QGridLayout(self.grid_container)
        self.nav_container = QtWidgets.QWidget()
        self.navLayout = QtWidgets.QHBoxLayout(self.nav_container)
        self.edit_page = QtWidgets.QLineEdit()

        self.filter_container = QtWidgets.QWidget()
        self.filter_container.setVisible(False)

        self.filter_container.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                                                  QtWidgets.QSizePolicy.Fixed))
        self.nav_container.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                                               QtWidgets.QSizePolicy.Fixed))
        self.grid_container.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored,
                                                                QtWidgets.QSizePolicy.Ignored))
        filter_layout = QtWidgets.QHBoxLayout(self.filter_container)
        self.edit_filter = QtWidgets.QLineEdit()
        self.combo_filter = QtWidgets.QComboBox()
        self.combo_filter.addItem("Title")
        self.combo_filter.addItem("Actress")
        btn_filter = QtWidgets.QPushButton("Filter")
        btn_filter.clicked.connect(self.filter_results)
        btn_filter_sort = QtWidgets.QPushButton("Sort")
        btn_filter_sort.clicked.connect(self.sort_result)
        filter_layout.addWidget(self.edit_filter)
        filter_layout.addWidget(self.combo_filter)
        filter_layout.addWidget(btn_filter)
        filter_layout.addWidget(btn_filter_sort)
        # self.anotherLayout.setSpacing(0)
        self.anotherLayout.addWidget(self.filter_container)
        self.anotherLayout.addWidget(self.grid_container)
        self.anotherLayout.addWidget(self.nav_container)

        self.ui.btn_search.clicked.connect(self.search)
        self.ui.btn_add_video.clicked.connect(self.add_tab)

        self.db = VideoDatabase()

        self.init_gui()

        self.dir_path = None
        self.is_connected = False

        self.dir_path = ConfigFunctions.project_dir_read()
        db_path = self.dir_path + '/vidbro.db'
        self.is_connected = self.db.create_connection(db_path)
        self.db.create_tables()
        if self.dir_path is None:
            msg_box = QMessageBox()
            msg_box.setText("Podaj folder projektu")
            msg_box.exec()
            self.change_project_dir()

        self.videos = None
        self.filtered = None
        self.current_page = 0
        self.vid_per_page = 100
        self.n_videos = 0
        self.n_pages = 0
        self.video_added = False

        self.search()

    def init_gui(self):
        self.gridLayout.setContentsMargins(5, 5, 5, 5)
        self.gridLayout.setObjectName("gridLayout")

        scroll_widget_policy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored,
                                                     QtWidgets.QSizePolicy.Ignored)
        self.ui.scrollAreaWidgetContents.setMinimumHeight(300)
        self.ui.scrollAreaWidgetContents.setMinimumWidth(500)
        self.ui.scrollAreaWidgetContents.setMaximumHeight(2200)
        self.ui.scrollAreaWidgetContents.setMaximumWidth(1000)
        self.ui.scrollAreaWidgetContents.setSizePolicy(scroll_widget_policy)
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)

        extractAction = QAction("&Project directory", self)
        extractAction.triggered.connect(self.change_project_dir)
        addVideoAction = QAction("&Add video", self)
        addVideoAction.triggered.connect(self.add_video)
        addActAction = QAction("&Add actress", self)
        addActAction.triggered.connect(self.add_actress)
        searchActAction = QAction("&Search actress", self)
        searchActAction.triggered.connect(self.view_actress)

        videosInPathAction = QAction("&Videos in path", self)
        videosInPathAction.triggered.connect(self.videos_in_path)

        batchMove = QAction("&Batch move", self)
        batchMove.triggered.connect(self.batch_move)

        aboutAction = QAction("&About", self)
        aboutAction.triggered.connect(about)

        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('File')
        funcMenu = mainMenu.addMenu('Functions')
        toolsMenu = mainMenu.addMenu('Tools')
        helpMenu = mainMenu.addMenu('Help')
        fileMenu.addAction(extractAction)
        funcMenu.addAction(addVideoAction)
        funcMenu.addAction(searchActAction)
        funcMenu.addAction(addActAction)
        toolsMenu.addAction(videosInPathAction)
        toolsMenu.addAction(batchMove)
        helpMenu.addAction(aboutAction)

        btn_prev = QtWidgets.QPushButton("<")
        btn_next = QtWidgets.QPushButton(">")
        btn_prev.clicked.connect(self.prev_page)
        btn_next.clicked.connect(self.next_page)
        self.navLayout.addWidget(btn_prev)
        self.navLayout.setAlignment(btn_prev, Qt.AlignRight)
        self.navLayout.addWidget(self.edit_page)
        self.navLayout.setAlignment(self.edit_page, Qt.AlignHCenter)
        self.navLayout.addWidget(btn_next)
        self.navLayout.setAlignment(btn_next, Qt.AlignLeft)
        self.ui.combo_search.addItem("Title")
        self.ui.combo_search.addItem("Actress")

    def change_project_dir(self):
        dialog = QtWidgets.QFileDialog()
        dialog.setFileMode(QtWidgets.QFileDialog.Directory)
        dialog.setOption(QtWidgets.QFileDialog.ShowDirsOnly)
        self.dir_path = dialog.getExistingDirectory(self, "Chose Directory", "K://Sekretny Folder")
        ConfigFunctions.project_dir_write(self.dir_path)
        db_path = self.dir_path + '/vidbro.db'
        if self.db.is_connection():
            self.db.close_connection()
        self.is_connected = self.db.create_connection(db_path)
        self.db.create_tables()

    def videos_in_path(self):
        p = SearchPath()
        p.exec()

    def batch_move(self):
        b = BatchMoveModel()
        b.exec()
        self.search()

    @functions.db_conn_decorator
    def add_tab(self):
        v = AddTab(self.db, self.dir_path)
        v.exec()
        self.video_added = True
        self.search()

    @functions.db_conn_decorator
    def add_video(self):
        v = AddVideo(self.db, self.dir_path)
        v.exec()
        self.video_added = True
        self.search()

    @functions.db_conn_decorator
    def add_actress(self):
        a = AddActress(self.db)
        a.exec()

    @functions.db_conn_decorator
    def view_actress(self):
        va = ViewActress()
        va.exec()

    @functions.db_conn_decorator
    def search(self):
        selection = None
        if self.ui.combo_search.currentText() == "Title":
            selection = self.db.select_video_by_title
        elif self.ui.combo_search.currentText() == "Actress":
            selection = self.db.select_video_by_actress
        search_text = self.ui.edit_search.text()
        if search_text:
            self.videos = selection(search_text)
        else:
            self.videos = self.db.select_all_video()
        print("App.py: ", self.videos[1].view_count, self.videos[1].last_view)
        self.filtered = self.videos
        self.n_videos = len(self.videos)
        self.n_pages = self.n_videos // self.vid_per_page
        if self.video_added:
            self.edit_page.setText("{}".format(self.n_pages))
            self.current_page = self.n_pages
        else:
            self.edit_page.setText("0")
            self.current_page = 0
        self.video_added = False
        self.filter_container.setVisible(True)
        self.show_videos()

    def show_videos(self):
        page_start = self.current_page*self.vid_per_page
        page_end = page_start + self.vid_per_page
        if page_end > self.n_videos:
            page_end = self.n_videos
        pagin = self.filtered[page_start:page_end]

        rows = len(pagin)//5 + 1
        functions.clear_layout(self.gridLayout)
        for i in range(rows):
            for j in range(5):
                if i * 5 + j < len(pagin):
                    video = pagin[i*5+j]
                    widget = SmallWidget(video)
                    widget.setMinimumSize(128, 72)
                    widget.setMaximumSize(256, 144)
                    self.gridLayout.addWidget(widget, i, j)
                else:
                    break
        self.grid_container.adjustSize()

    @functions.db_conn_decorator
    def next_page(self):
        if self.current_page < self.n_pages:
            self.current_page += 1
            self.edit_page.setText(str(self.current_page))
            self.show_videos()

    @functions.db_conn_decorator
    def prev_page(self):
        if self.current_page > 0:
            self.current_page -= 1
            self.edit_page.setText(str(self.current_page))
            self.show_videos()

    def sort_result(self):
        self.filtered.sort(key=functions.compare_title)
        self.show_videos()

    def filter_results(self):
        result = self.edit_filter.text()
        if result:
            filter_type = self.combo_filter.currentText()
            new_videos = []
            if filter_type == "Title":
                # TODO filtrowanie po tytule
                pass
            elif filter_type == "Actress":
                act_vids = self.db.select_video_by_actress(result)
                for va in act_vids:
                    for v in self.videos:
                        if va.title == v.title:
                            new_videos.append(v)
            self.filtered = new_videos
            self.show_videos()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Return:
            self.search()

    def closeEvent(self, event):
        self.db.close_connection()
        print('DB closed')


app = QtWidgets.QApplication([])
app.setStyle('Fusion')
application = MainApp()
application.show()
sys.exit(app.exec())
