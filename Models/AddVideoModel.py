from GUIs.AddVideo import Ui_Dialog
from PyQt5.QtWidgets import QDialog, QFileDialog, QLabel, QVBoxLayout, QSpacerItem, QSizePolicy, QFrame
from PyQt5.QtGui import QPixmap, QImageWriter
from PyQt5.QtCore import QDir, Qt, QStringListModel
from Players.ThumbnailPlayer import ThumbBrowser
from Models.ChoseActressModel import ChoseActress
from Database.Video import Video
from Misc.functions import print_warning, delete_file, files_used
from Misc.CopyFileThread import CopyFileThread
from Misc import ConfigFunctions
from threading import Thread
from time import time
import os
import gc


class AddVideo(QDialog):
    def __init__(self, db, dir_path):
        super(AddVideo, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.db = db
        self.setWindowModality(Qt.ApplicationModal)
        self.project_dir = dir_path
        lastdir = ConfigFunctions.config_section_read('project_folder', 'last_video_dir')
        if lastdir:
            self.last_dir = lastdir
        else:
            self.last_dir = dir_path
        self.video_name = None
        self.video_path = None
        self.src_video_path = None
        self.thumbnail = None
        self.video_duration = None
        self.video_res_x = None
        self.video_res_y = None
        self.codec = None
        self.copy = False
        self.ui.btn_browse.clicked.connect(self.browse)
        self.ui.btn_add.clicked.connect(self.add_video)
        self.ui.btn_thumb.clicked.connect(self.browse_thumbnail)
        self.ui.btn_add_act.clicked.connect(self.add_actress)
        self.ui.btn_del_act.clicked.connect(self.del_actress)
        self.ui.list_act.clicked.connect(self.item_clicked)
        self.ui.check_copy.stateChanged.connect(self.copy_selected)
        self.ui.btn_thumb.setDisabled(True)
        self.ui.btn_add_act.setDisabled(True)
        self.ui.btn_del_act.setDisabled(True)
        self.ui.btn_add.setDisabled(True)
        self.ui.check_delete.setDisabled(True)
        self.progress = False
        self.act_list = []
        self.current_index = None
        self.act_list_model = QStringListModel(self)
        act_names = []  # TODO to jest coś podejrzane
        self.act_list_model.setStringList(act_names)
        self.ui.list_act.setModel(self.act_list_model)
        self.filesize = 0
        self.cpVideo = None
        self.info_list = []
        self.ver_layout = QVBoxLayout()
        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        # self.ver_layout.addSpacerItem(self.verticalSpacer)
        self.ui.scroll_info.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.ui.scroll_info.setWidgetResizable(True)
        self.ui.widget_scroll_info.setLayout(self.ver_layout)
        self.initial_scroll_bar_size = self.ui.scroll_info.verticalScrollBar().maximum()
        self.count = 0
        self.ui.scroll_info.verticalScrollBar().rangeChanged.connect(self.range_changed)
        self.has_range_changed = False
        self.last_time = time()

    def add_info_text(self, text):
        new_info = QLabel(text)
        # new_info.setStyleSheet("QLabel { background-color : grey; color : red; }")
        self.count += 1
        new_info.setWordWrap(True)
        new_info.setFrameStyle(QFrame.StyledPanel)
        self.ver_layout.removeItem(self.verticalSpacer)
        self.info_list.append(new_info)
        self.ver_layout.addWidget(new_info)
        if not self.has_range_changed:
            self.ver_layout.addSpacerItem(self.verticalSpacer)

    def range_changed(self, new_min, new_max):
        ver_scroll_bar = self.ui.scroll_info.verticalScrollBar()
        ver_scroll_bar.setSliderPosition(new_max)
        if not self.has_range_changed:
            self.has_range_changed = True
            self.ver_layout.removeItem(self.verticalSpacer)

    def add_video(self):
        self.video_name = self.ui.edit_video_name.text()
        if self.video_name and self.video_path and self.thumbnail:
            if self.db.is_video_in_base(self.video_path):
                print_warning("Taki film znajduje się już w bazie")
            else:
                try:
                    self.src_video_path = self.video_path
                    if self.copy:
                        self.cpVideo = CopyFileThread(self.video_path, self.project_dir)
                        self.filesize = os.stat(self.video_path).st_size
                        self.cpVideo.copied.connect(self.copy_progress)
                        self.cpVideo.finished.connect(self.thread_finished)
                        self.cpVideo.msg.connect(self.add_info_text)
                        self.video_path = self.cpVideo.get_dst_path()
                        self.cpVideo.start()
                        filesize_in_mb = str(int(self.filesize/(1024*1024)))
                        self.add_info_text("Kopiowanie rozpoczęte. Rozmiar pliku {}".format(filesize_in_mb))
                    else:
                        self.add_video_to_db()
                        self.add_info_text("Film " + self.video_name + "został dodany")
                        self.clean_after_addition()
                except Exception as e:
                    print("AddVideoModel: ", e)
        else:
            info_str = "Podaj "
            if not self.video_name:
                info_str = info_str + "nazwę "
            if not self.thumbnail:
                info_str = info_str + "miniaturkę "
            if not self.video_path:
                info_str = info_str + "ścieżkę do "
            info_str = info_str + "filmu"
            print_warning(info_str)

    def fill_video(self):
        producer = self.ui.edit_producer.text()
        thumb_dir = QDir(self.project_dir)
        thumb_dir.mkdir('thumbnails')
        thumb_path = self.project_dir + '/thumbnails/' + self.video_name + '.jpeg'
        imwrite = QImageWriter(thumb_path)
        self.thumbnail = self.thumbnail.scaled(512, 288, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        imwrite.write(self.thumbnail)
        v = Video()
        v.title = self.video_name
        v.path = self.video_path
        v.producer = producer
        v.thumb = thumb_path
        v.duration = self.video_duration
        v.resolution_x = self.video_res_x
        v.resolution_y = self.video_res_y
        v.compression = self.codec
        v.view_count = 0
        v.last_view = int(time())
        return v

    def browse(self):
        dialog = QFileDialog(self)
        dialog.setFileMode(QFileDialog.ExistingFile)
        path, opt = dialog.getOpenFileName(self, "Chose Video", self.last_dir,
                                           "Video (*.mp4 *.mkv *.wmv *.mov *.avi)")
        if path:
            self.video_path = path
            split_video_name = os.path.splitext(os.path.abspath(path))[0]
            self.last_dir = os.sep.join(split_video_name.split(os.sep)[:-2])
            ConfigFunctions.config_section_write('project_folder', 'last_video_dir', self.last_dir)
            video_file_name = split_video_name.split(os.sep)[-1]
            cleaned_video_name = video_file_name.replace('_', ' ')
            cleaned_video_name = cleaned_video_name.replace('-', ' ')
            cleaned_video_name = cleaned_video_name.replace('.', ' ')
            cleaned_video_name = " ".join(cleaned_video_name.split())  # Remove many spaces
            self.ui.edit_video_name.setText(cleaned_video_name)
            self.ui.label_browse.setText(path)
            self.ui.btn_thumb.setEnabled(True)
            self.ui.btn_add_act.setEnabled(True)
            self.ui.btn_add.setEnabled(True)
            self.ui.label_thumb.clear()
            self.thumbnail = None
            # Oczekiwanie na unifikacje playerów
            # self.thumb_browser.default_thumb()

    def clean_after_addition(self):
        self.ui.edit_video_name.clear()
        self.ui.edit_producer.clear()
        self.ui.label_browse.clear()
        self.ui.label_thumb.clear()
        self.thumbnail = None
        self.video_name = None
        self.video_path = None
        self.ui.btn_thumb.setEnabled(False)
        self.ui.btn_add_act.setEnabled(False)
        self.ui.btn_add.setEnabled(False)
        self.act_list = []
        act_list_row_cnt = self.act_list_model.rowCount()
        for i in reversed(range(act_list_row_cnt)):
            self.act_list_model.removeRow(i)

    def browse_thumbnail(self):
        try:
            files_used(self.video_path, "AddVideoModel before browse_thumbnail")
            thumb_browser = ThumbBrowser()
            thumb_browser.set_video(self.video_path)
            thumb_browser.exec()
            info = thumb_browser.get_vid_info()
            self.video_duration = info[0]
            self.video_res_x = info[1]
            self.video_res_y = info[2]
            self.codec = info[3]
            self.thumbnail = thumb_browser.return_image()
            del thumb_browser
            gc.collect()
            files_used(self.video_path, "AddVideoModel browse_thumbnail")
            self.ui.label_thumb.setPixmap(QPixmap.fromImage(self.thumbnail).scaled(
                    self.ui.label_thumb.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        except Exception as e:
            self.thumbnail = None
            print("AddVideoModel, browse_thumbnail: ", e)

    def add_actress(self):
        a = ChoseActress()
        a.exec()
        result = a.return_result()
        if result:
            added_act = ""
            for act in self.act_list:
                for res in result:
                    if act.name == res.name:
                        added_act += act.name + ", "
                        result.remove(res)
                        break
            if added_act != "":
                print_warning("Aktorki: "+added_act+"zostały już dodane")
            if result:
                for act in result:
                    self.act_list.append(act)
                    self.act_list_model.insertRow(self.act_list_model.rowCount())
                    index = self.act_list_model.index(self.act_list_model.rowCount()-1)
                    self.act_list_model.setData(index, act.name)

    def del_actress(self):
        self.act_list.pop(self.current_index.row())
        self.act_list_model.removeRow(self.current_index.row())

    def item_clicked(self, index):
        self.current_index = index
        self.ui.btn_del_act.setEnabled(True)

    def copy_progress(self, progress):
        value = progress/(self.filesize/1024) * 100
        self.ui.progressBar.setValue(value)
        prog_in_mb = str(int(progress/1024))
        filesize_in_mb = str(int(self.filesize/(1024*1024)))
        new_time = time()
        if new_time - self.last_time > 1:
            self.last_time = new_time
            self.add_info_text("Kopiowanie(" + prog_in_mb + "/" + filesize_in_mb + "MB)")

    def add_video_to_db(self):
        v = self.fill_video()
        vid_id = self.db.insert_video(v)
        for act in self.act_list:
            self.db.insert_actress_video(vid_id, act.act_id)
        self.ui.label_thumb.clear()
        self.thumbnail = None
        print("Dodano film", self.video_name)

    def thread_finished(self):
        self.add_video_to_db()
        if self.ui.check_delete.isChecked():
            print("Usuwanie pliku ", self.src_video_path)
            #files_used(self.src_video_path, "AddVideoModel thread_finished")
            x = Thread(target=delete_file, args=(self.src_video_path,))
            x.start()
        self.clean_after_addition()

    def copy_selected(self):
        self.ui.check_delete.setEnabled(self.ui.check_copy.isChecked())
        self.copy = self.ui.check_copy.isChecked()
