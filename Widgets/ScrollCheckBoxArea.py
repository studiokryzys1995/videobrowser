from PyQt5.QtWidgets import QScrollArea, QWidget, QVBoxLayout, QSizePolicy
from Misc.functions import clear_layout


class ScrollCheckBoxArea(QScrollArea):
    def __init__(self, context):
        super().__init__(context)
        self.setWidgetResizable(True)
        self.main_widget = QWidget()
        self.main_widget.setSizePolicy(QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum))
        self.setWidget(self.main_widget)
        self.main_layout = QVBoxLayout()
        self.main_widget.setLayout(self.main_layout)
        self.widget_list = []

    def add_entry(self, widget):
        self.main_layout.addWidget(widget)
        self.widget_list.append(widget)

    def clean_entries(self):
        clear_layout(self.main_layout)
        self.widget_list.clear()

    def get_checked_widgets(self):
        checked_list = []
        for widget in self.widget_list:
            if widget.is_checked():
                checked_list.append(widget.get_data())
        return checked_list
