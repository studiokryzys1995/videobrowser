from PyQt5.QtMultimedia import QAbstractVideoSurface, QVideoFrame, QAbstractVideoBuffer, QVideoSurfaceFormat
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import QRect, QPoint, Qt
import timeit


class ThumbnailSurface(QAbstractVideoSurface):
    def __init__(self, widget):
        super(ThumbnailSurface, self).__init__()
        self.widget = widget
        self.image_format = None
        self.target_rect = None
        self.image_size = None
        self.source_rect = None
        self.current_frame = None

    def supportedPixelFormats(self, handleType=QAbstractVideoBuffer.NoHandle):
        formats = [QVideoFrame.PixelFormat()]
        if handleType == QAbstractVideoBuffer.NoHandle:
            for f in [QVideoFrame.Format_RGB32,
                      QVideoFrame.Format_ARGB32,
                      QVideoFrame.Format_ARGB32_Premultiplied,
                      QVideoFrame.Format_RGB24,
                      QVideoFrame.Format_RGB565,
                      QVideoFrame.Format_RGB555]:
                formats.append(f)
        return formats

    def isFormatSupported(self, sformat):
        imageFormat = QVideoFrame.imageFormatFromPixelFormat(sformat.pixelFormat())
        size = sformat.frameSize()

        return imageFormat != QImage.Format_Invalid \
            and not size.isEmpty() \
            and sformat.handleType() == QAbstractVideoBuffer.NoHandle

    def start(self, sformat):
        image_format = QVideoFrame.imageFormatFromPixelFormat(sformat.pixelFormat())
        size = sformat.frameSize()
        if image_format != QImage.Format_Invalid and not size.isEmpty():
            self.image_format = image_format
            self.image_size = size
            self.source_rect = sformat.viewport()

            super().start(sformat)

            self.updateVideoRect()

            return True
        else:
            print("Nie udało się")
            return False

    def stop(self):
        self.current_frame = QVideoFrame()
        self.target_rect = QRect()
        super().stop()
        self.widget.update()

    def present(self, frame):
        if (self.surfaceFormat().pixelFormat() != frame.pixelFormat() or
                self.surfaceFormat().frameSize() != frame.size()):
            print("Nie udało się")
            self.setError(QAbstractVideoSurface.IncorrectFormatError)
            self.stop()
            return False
        else:
            self.current_frame = frame
            self.widget.update(self.target_rect)

            return True

    def updateVideoRect(self):
        size = self.surfaceFormat().sizeHint()
        size.scale(self.widget.size().boundedTo(size), Qt.KeepAspectRatio)
        self.target_rect = QRect(QPoint(0, 0), size)
        self.target_rect.moveCenter(self.widget.rect().center())

    def videoRect(self):
        return self.target_rect

    def isNewFrame(self):
        if self.current_frame:
            return True
        else:
            return False

    def paint(self, painter):
        if self.current_frame.map(QAbstractVideoBuffer.ReadOnly):
            oldTransform = painter.transform()
            if self.surfaceFormat().scanLineDirection() == QVideoSurfaceFormat.BottomToTop:
                painter.scale(1, -1)
                painter.translate(0, -self.widget.height())

            image = QImage(self.current_frame.bits(),
                           self.current_frame.width(),
                           self.current_frame.height(),
                           self.current_frame.bytesPerLine(),
                           QVideoFrame.imageFormatFromPixelFormat(self.current_frame.pixelFormat()))

            painter.drawImage(self.target_rect, image)

            painter.setTransform(oldTransform)

            self.current_frame.unmap()

    def return_current_frame(self):
        if self.current_frame:
            image = None
            if self.current_frame.map(QAbstractVideoBuffer.ReadOnly):

                image = QImage(self.current_frame.bits(),
                               self.current_frame.width(),
                               self.current_frame.height(),
                               self.current_frame.bytesPerLine(),
                               QVideoFrame.imageFormatFromPixelFormat(self.current_frame.pixelFormat()))

                self.current_frame.unmap()
                return image.copy()
            else:
                return image
