from PyQt5.QtCore import QThread, pyqtSignal
from Misc.Copy import *
from Misc.functions import file_md5, files_used, create_video_dir_proj_path
from shutil import disk_usage


class CopyFileThread(QThread):
    copied = pyqtSignal(float)
    finished = pyqtSignal()
    msg = pyqtSignal(str)

    def __init__(self, path, project_dir):
        QThread.__init__(self)
        self.src_path = path
        self.pr_dir = project_dir

        self.dst_path = create_video_dir_proj_path(self.src_path)
        print(self.dst_path)

    def get_dst_path(self):
        return self.dst_path

    def run(self):
        files_used(self.src_path, "CopyFile Thread before open")
        src = open(self.src_path, "br")
        file_size = os.stat(src.fileno()).st_size
        (total, used, free) = disk_usage(self.pr_dir)
        if file_size > free:
            self.msg.emit("Brak miejsca na dysku")
            src.close()
            return
        dst = open(self.dst_path, "bw")
        copyfileobj(src, dst, self.copied, 16*1024)
        self.msg.emit("Porównywanie plików")
        # print("Porównywanie plików")
        src.close()
        dst.close()
        files_used(self.src_path, "CopyFile Thread")
        cmp_result = file_md5(self.src_path) == file_md5(self.dst_path)
        # print("Wynik porównania: ", cmp_result)
        if cmp_result:
            self.msg.emit("Kopiowanie zakonczone")
            self.finished.emit()
        else:
            self.msg.emit("Nastąpił błąd przy kopiowaniu")
            # TODO rozbudowanie tego komunikatu
