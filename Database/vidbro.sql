CREATE TABLE IF NOT EXISTS VIDEO (
    vid_id INTEGER PRIMARY KEY,
    title TEXT NOT NULL,
    producer TEXT,
    path TEXT NOT NULL,
    thumb TEXT,
    duration INTEGER,
    resolution_x INTEGER,
    resolution_y INTEGER,
    gens_id INTEGER,
    mark INTEGER,
    favourite INTEGER,
    last_view INTEGER,
    view_count INTEGER,
    compression TEXT,
    archive TEXT
);

CREATE TABLE IF NOT EXISTS ACTRESS (
    act_id INTEGER PRIMARY KEY,
    name TEXT,
    date TEXT,
    origins TEXT,
    avatar TEXT
);

CREATE TABLE IF NOT EXISTS ACTRESS_VIDEOS (
    vid_id INTEGER NOT NULL,
    act_id INTEGER NOT NULL,
    PRIMARY KEY (vid_id, act_id),
    FOREIGN KEY (vid_id)
        REFERENCES VIDEO (vid_id)
            ON DELETE CASCADE
            ON UPDATE NO ACTION,
    FOREIGN KEY (act_id)
        REFERENCES ACTRESS (act_id)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS PHOTOS (
    ph_id INTEGER PRIMARY KEY,
    act_id INTEGER,
    path TEXT,
    album TEXT
);

CREATE TABLE IF NOT EXISTS VID_GENRES (
    id INTEGER PRIMARY KEY,
    gen_id INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS GENRE (
    id INTEGER PRIMARY KEY,
    name TEXT
);
