from configparser import ConfigParser, NoSectionError, NoOptionError, DuplicateSectionError, DuplicateOptionError


def get_config_parser():
    pass
    # if config is None:
    #     config = ConfigParser()
    #     config.read('config.ini')
    # Czeka aż z config parsera zrobi się klasę i z niej singletona żeby nie tworzyć za każdym razem


def string_section_option(section, option, value):
    text = "section:=" + section + " option:=" + option + " value:=" + value
    return text


def project_dir_write(dir_path):
    if dir_path:
        config_section_write('project_folder', 'folder', dir_path)
    else:
        print("ConfigFunctions: ", "You must chose project directory")


def config_section_write(section, option, value):
    config = ConfigParser()
    config.read('config.ini')

    try:
        if section not in config.sections():
            config.add_section(section)
        config.set(section, option, value)
        with open('config.ini', 'w') as f:
            config.write(f)
        print("ConfigFunctions:", string_section_option(section, option, value), " saved!")
    except (DuplicateSectionError, DuplicateOptionError) as e:
        print("App: ", e)


def project_dir_read():
    read = config_section_read('project_folder', 'folder')
    return read


def config_section_read(section, option):
    config = ConfigParser()
    config.read('config.ini')

    try:
        if section not in config.sections():
            print(section, " Is not set !")
            return None
        else:
            value = config.get(section, option)
            print("ConfigFunctions:", string_section_option(section, option, value))
            return value
    except (NoSectionError, NoOptionError) as e:
        print("ConfigFunctions: ", e)