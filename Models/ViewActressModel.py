from Models.SearchActressModel import SearchActress
from Models.ActresPageModel import ActressPage


class ViewActress(SearchActress):
    def __init__(self):
        super(ViewActress, self).__init__()
        self.ui.list_search.doubleClicked.connect(self.item_dclicked)

    def item_dclicked(self, index):
        for act in self.search_result:
            if act.name == index.data():
                self.selected = act
        ap = ActressPage(self.selected)
        ap.exec()
