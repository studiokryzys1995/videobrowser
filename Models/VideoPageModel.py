from PyQt5.QtWidgets import QDialog
from PyQt5.QtGui import QPixmap, QImageWriter
from PyQt5.QtCore import Qt, QStringListModel
from GUIs.VideoPage import Ui_Dialog
from Database.Database import VideoDatabase
from Players.ThumbnailPlayer import ThumbBrowser
from Misc import functions, ConfigFunctions
from Models.ChoseActressModel import ChoseActress
from Models.ChangeLocationModel import ChangeLocation
import os


class VideoPage(QDialog):
    def __init__(self, video):
        super(VideoPage, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.db = VideoDatabase()
        self.video = video
        self.project_dir = ConfigFunctions.project_dir_read()
        thumbnail = QPixmap(video.thumb)
        self.ui.label_thumb.setPixmap(thumbnail.scaled(self.ui.label_thumb.size(),
                                                       Qt.KeepAspectRatio, Qt.SmoothTransformation))
        self.ui.edit_title.setText(self.video.title)
        self.ui.edit_producer.setText(self.video.producer)
        self.ui.edit_mark.setText(self.video.mark)
        self.ui.label_duration.setText(functions.duration_to_sting(self.video.duration))
        self.ui.label_path.setText(functions.add_spaces_to_text(27, self.video.path))
        if self.video.resolution_x and self.video.resolution_y:
            self.ui.label_resolution.setText(str(self.video.resolution_x)+"x"+str(self.video.resolution_y))
        else:
            self.ui.label_resolution.setText("0x0")
        self.ui.label_format.setText(os.path.splitext(video.path)[1])
        self.ui.label_codec.setText(self.video.compression)

        self.ui.btn_change_thumb.clicked.connect(self.change_thumbnail)
        self.ui.btn_cancel.clicked.connect(self.cancel)
        self.ui.btn_ok.clicked.connect(self.confirm)
        self.ui.btn_add.clicked.connect(self.add_actress)
        self.ui.btn_remove.clicked.connect(self.del_actress)
        self.ui.btn_remove.setEnabled(False)
        self.ui.list_cast.clicked.connect(self.item_clicked)
        self.ui.btn_new_location.clicked.connect(self.move_video_to_new_location)
        self.actresses = []
        self.initial_actresses = None
        self.current_index = None
        self.act_list_model = QStringListModel(self)
        self.actress_list()

    def change_thumbnail(self):
        pass
        thumb = None
        try:
            browser = ThumbBrowser()
            browser.show()
            browser.set_video(self.video.path)
            browser.exec()
            thumb = browser.return_image()
            self.ui.label_thumb.setPixmap(QPixmap.fromImage(thumb).scaled(
                    self.ui.label_thumb.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        except Exception as e:
            print("VideoPageModel: ", e)
        if thumb:
            thumb = thumb.scaled(512, 288, Qt.KeepAspectRatio, Qt.SmoothTransformation)
            thumb_path = self.video.thumb
            os.remove(thumb_path)
            imwrite = QImageWriter(thumb_path)
            imwrite.write(thumb)

    def cancel(self):
        self.close()

    def update_video_args(self):
        self.video.title = self.ui.edit_title.text()
        self.video.producer = self.ui.edit_producer.text()
        # dodanie oceny

    def update_video_actresses(self):
        act_removed = []
        for act in self.initial_actresses:
            if act not in self.actresses:
                act_removed.append(act)
        act_added = []
        for act in self.actresses:
            if act not in self.initial_actresses:
                act_added.append(act)
        for act in act_added:
            self.db.insert_actress_video(self.video.vid_id, act.act_id)
        for act in act_removed:
            self.db.delete_actress_video(self.video.vid_id, act.act_id)

    def move_video_to_new_location(self):
        ch = ChangeLocation(self.video)
        ch.exec()
        self.ui.label_path.setText(functions.add_spaces_to_text(27, self.video.path))

    def confirm(self):
        self.update_video_args()
        self.db.update_video(self.video)
        self.update_video_actresses()
        self.close()

    def actress_list(self):
        self.actresses = self.db.select_video_actresses(self.video.vid_id)
        self.initial_actresses = self.actresses.copy()
        titles = []
        for act in self.actresses:
            titles.append(act.name)
        #  titles.sort()
        self.act_list_model.setStringList(titles)
        self.ui.list_cast.setModel(self.act_list_model)

    # def item_dclicked(self, index):
    #     for act in self.actresses:
    #         if act.name == index.data():
    #             act_page = ActressPage(act)
    #             act_page.exec()

    def add_actress(self):
        a = ChoseActress()
        a.exec()
        result = a.return_result()
        if result:
            added_act = ""
            for act in self.actresses:
                for res in result:
                    if act.name == res.name:
                        added_act += act.name + ", "
                        result.remove(res)
                        break
            if added_act != "":
                functions.print_warning("Aktorki: "+added_act+"zostały już dodane")
            if result:
                for act in result:
                    self.actresses.append(act)
                    self.act_list_model.insertRow(self.act_list_model.rowCount())
                    index = self.act_list_model.index(self.act_list_model.rowCount()-1)
                    self.act_list_model.setData(index, act.name)

    def del_actress(self):
        self.actresses.pop(self.current_index.row())
        self.act_list_model.removeRow(self.current_index.row())

    def item_clicked(self, index):
        self.current_index = index
        self.ui.btn_remove.setEnabled(True)

