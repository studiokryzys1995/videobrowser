from GUIs.AddActress import Ui_Dialog
from PyQt5.QtWidgets import QDialog, QFileDialog, QMessageBox
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
from Misc.ConfigFunctions import project_dir_read


class AddActress(QDialog):
    def __init__(self, db):
        super(AddActress, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.btn_add.clicked.connect(self.add_actress)
        self.ui.btn_avatar.clicked.connect(self.browse_avatar)
        self.ui.btn_cancel.clicked.connect(self.cancel)
        self.photo_path = None
        self.db = db

    def browse_avatar(self):
        dialog = QFileDialog()
        project_dir = project_dir_read()
        # TODO Dodaj folder Avatars jak nie istnieje
        path, opt = dialog.getOpenFileName(self, "Chose avatar", project_dir+"/Avatars")
        if path:
            self.photo_path = path
            photo = QPixmap(path).scaled(self.ui.label_avatar.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.ui.label_avatar.setPixmap(photo)

    def add_actress(self):
        name = self.ui.edit_name.text()
        origins = self.ui.edit_origins.text()
        date = self.ui.dateEdit.date().toString()
        if name and not self.db.is_actress_in_base(name):
            self.db.insert_actress(name, date, origins, self.photo_path)
            print("Dodano aktorkę", name)
            self.clean_after_addition()
        else:
            msg_box = QMessageBox()
            info_str = ""
            if not name:
                info_str = "Podaj pseudonim aktorki"
            elif self.db.is_actress_in_base(name):
                info_str = "Taka aktorka znajduje się w bazie"
            msg_box.setText(info_str)
            msg_box.exec()

    def clean_after_addition(self):
        self.ui.edit_name.clear()
        self.ui.edit_origins.clear()
        self.ui.label_avatar.clear()
        self.photo_path = None

    def cancel(self):
        self.close()
