from Widgets.CheckBoxTextWidget import CheckBoxTextWidget


class CheckBoxVideoWidget(CheckBoxTextWidget):
    def __init__(self, video):
        super().__init__(video.title)
        self.video = video

    def get_data(self):
        return self.video

