from Players.AbstractPlayer import Player
from PyQt5.QtCore import Qt, QEvent, QTimer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from Widgets.VideoWidget import VideoWidget
from Database.Database import VideoDatabase
from time import time


class VideoPlayer(Player):
    def __init__(self, video):
        super(VideoPlayer, self).__init__()
        #self.videoWidget = VideoWidget(self)
        self.videoWidget = QVideoWidget(self)
        self.video = video

        self.setObjectName("VideoPlayer")
        self.setWindowModality(Qt.ApplicationModal)

        #self.set_video_surface(self.videoWidget.video_widget)
        self.set_video_surface(self.videoWidget)

        self.layout.addWidget(self.videoWidget)
        self.init_layout()

        self.videoWidget.installEventFilter(self)
        self.videoWidget.setMouseTracking(True)
        self.timer_mouse_dc = QTimer()
        self.timer_mouse_dc.setSingleShot(True)
        self.timer_mouse_dc.timeout.connect(self.play)
        self.timer_mouse_hide = QTimer()
        self.timer_mouse_hide.setSingleShot(True)
        self.timer_mouse_hide.timeout.connect(self.hide_cursor)

        self.set_title(self.video.title)
        self.set_video(self.video.path)

    def set_title(self, title):
        self.setWindowTitle(title)

    def hide_cursor(self):
        self.videoWidget.setCursor(Qt.BlankCursor)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.MouseButtonDblClick:
            if self.videoWidget.isFullScreen():
                self.videoWidget.setFullScreen(False)
                self.videoWidget.setCursor(Qt.ArrowCursor)
                self.timer_mouse_hide.stop()
            else:
                self.videoWidget.setFullScreen(True)
            self.timer_mouse_dc.stop()
            return True
        if event.type() == QEvent.MouseButtonPress:
            if self.timer_mouse_dc.isActive():
                pass
                # Przycisk zostal wcześniej klikniety, mniej niż x ms temu
            else:
                self.timer_mouse_dc.start(250)
            return True
        if event.type() == QEvent.MouseMove:
            if self.videoWidget.isFullScreen():
                self.videoWidget.setCursor(Qt.ArrowCursor)
                self.timer_mouse_hide.start(2000)
            return True
        if event.type() == QEvent.KeyPress:
            if event.key() == Qt.Key_Escape:
                self.videoWidget.setFullScreen(False)
                self.videoWidget.setCursor(Qt.ArrowCursor)
                self.timer_mouse_hide.stop()
            return True
        return False

    def exec(self):
        db = VideoDatabase()
        if self.video.view_count is not None:
            self.video.view_count += 1
        else:
            self.video.view_count = 1
        self.video.last_view = int(time())
        print("VideoPlayer: ", self.video.view_count, self.video.last_view)
        db.update_video(self.video)
        super().exec()
