from PyQt5.QtWidgets import QWidget, QSizePolicy
from PyQt5.QtCore import Qt, QEvent
from PyQt5.QtGui import QPalette, QPainter, QRegion
from Widgets.ThumbnailSurface import ThumbnailSurface


class VideoWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.context = parent
        self.setAutoFillBackground(False)
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_PaintOnScreen, True)

        palette = self.palette()
        palette.setColor(QPalette.Background, Qt.black)
        self.setPalette(palette)

        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        self.surface = ThumbnailSurface(self)

    def return_thumbnail(self):
        if self.surface:
            return self.surface.return_current_frame()

    def sizeHint(self):
        return self.surface.surfaceFormat().sizeHint()

    def mousePressEvent(self, event):
        if event.type() == QEvent.MouseButtonPress:
            self.context.play()

    def paintEvent(self, event):
        painter = QPainter(self)
        if self.surface.isActive():
            videoRect = self.surface.videoRect()

            if not videoRect.contains(event.rect()):
                region = event.region()
                region = region.subtracted(QRegion(videoRect))  # zmieniono substract na substracted

                brush = self.palette().window()

                for rect in region.rects():
                    painter.fillRect(rect, brush)

            if self.surface.isNewFrame():
                self.surface.paint(painter)
        else:
            painter.fillRect(event.rect(), self.palette().window())

    def resizeEvent(self, event):
        super().resizeEvent(event)
        self.surface.updateVideoRect()
