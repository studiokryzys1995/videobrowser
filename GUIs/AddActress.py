# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'P:\ProgramingProjects\PythonProjects\VideoBrowser\GUIs\AddActress.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(500, 650)
        self.btn_cancel = QtWidgets.QPushButton(Dialog)
        self.btn_cancel.setGeometry(QtCore.QRect(500, 200, 75, 23))
        self.btn_cancel.setObjectName("btn_cancel")
        self.btn_add = QtWidgets.QPushButton(Dialog)
        self.btn_add.setGeometry(QtCore.QRect(370, 590, 75, 23))
        self.btn_add.setObjectName("btn_add")
        self.label_avatar = QtWidgets.QLabel(Dialog)
        self.label_avatar.setGeometry(QtCore.QRect(90, 10, 321, 321))
        self.label_avatar.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.label_avatar.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label_avatar.setText("")
        self.label_avatar.setAlignment(QtCore.Qt.AlignCenter)
        self.label_avatar.setObjectName("label_avatar")
        self.btn_avatar = QtWidgets.QPushButton(Dialog)
        self.btn_avatar.setGeometry(QtCore.QRect(200, 340, 101, 23))
        self.btn_avatar.setObjectName("btn_avatar")
        self.label_name = QtWidgets.QLabel(Dialog)
        self.label_name.setGeometry(QtCore.QRect(70, 410, 61, 16))
        self.label_name.setObjectName("label_name")
        self.label_date = QtWidgets.QLabel(Dialog)
        self.label_date.setGeometry(QtCore.QRect(70, 460, 47, 13))
        self.label_date.setObjectName("label_date")
        self.label_origins = QtWidgets.QLabel(Dialog)
        self.label_origins.setGeometry(QtCore.QRect(70, 500, 71, 16))
        self.label_origins.setObjectName("label_origins")
        self.edit_name = QtWidgets.QLineEdit(Dialog)
        self.edit_name.setGeometry(QtCore.QRect(160, 410, 261, 20))
        self.edit_name.setObjectName("edit_name")
        self.edit_origins = QtWidgets.QLineEdit(Dialog)
        self.edit_origins.setGeometry(QtCore.QRect(160, 500, 261, 20))
        self.edit_origins.setObjectName("edit_origins")
        self.dateEdit = QtWidgets.QDateEdit(Dialog)
        self.dateEdit.setGeometry(QtCore.QRect(160, 460, 110, 22))
        self.dateEdit.setObjectName("dateEdit")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.btn_cancel.setText(_translate("Dialog", "Cancel"))
        self.btn_add.setText(_translate("Dialog", "Add"))
        self.btn_avatar.setText(_translate("Dialog", "Wybierz awatar"))
        self.label_name.setText(_translate("Dialog", "Pseudonim:"))
        self.label_date.setText(_translate("Dialog", "Data:"))
        self.label_origins.setText(_translate("Dialog", "Pochodzenie:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
