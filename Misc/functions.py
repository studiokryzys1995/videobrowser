from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import QDir
from hashlib import md5
from time import sleep
from Database.Database import VideoDatabase
from Misc.ConfigFunctions import project_dir_read
from sys import exc_info
from pathlib import Path
import os
import shutil
import psutil


def clear_layout(layout):  # TODO Probable memory leak, items could be removed from layouts but not from program
    if layout is not None:
        for cnt in reversed(range(layout.count())):
            item = layout.itemAt(cnt)
            widget = item.widget()
            if widget is not None:
                widget.deleteLater()
            else:
                clear_layout(item.layout())
            layout.removeItem(item)


def print_warning(msg):
    msg_box = QMessageBox()
    msg_box.setWindowTitle("Warning")
    msg_box.setText(msg)
    msg_box.exec()


def db_conn_decorator(func):

    def inner(instance):
        if instance.is_connected:
            func(instance)
    return inner


def duration_to_sting(duration):
    if duration:
        dur_left = duration // 1000
        h = dur_left // (60 * 60)
        dur_left = dur_left - h * 60 * 60
        m = dur_left // 60
        dur_left = dur_left - m * 60
        s = dur_left
        return "{:0>2}:{:0>2}:{:0>2}".format(h, m, s)
    else:
        return "--:--:--"


def compare_title(e):
    return e.title.lower()


def file_md5(fname):
    md5hash = md5()
    with open(fname, 'rb') as handle:
        for line in handle:
            md5hash.update(line)
    return md5hash.hexdigest()


def find_videos_in_path(searched_path, recursive):
    db = VideoDatabase()
    videos = db.select_all_video()
    found = []
    movies = []
    database = []

    subfolders, files = run_fast_scandir(searched_path, [".mp4", ".avi"], recursive=recursive)
    for file in files:
        movies.append(os.path.normpath(file))

    for video in videos:
        video_file_name = os.path.split(video.path)[1] # TODO czy to nie jest funcja file_with_ext_from_path
        database.append([video_file_name, video.path])

    for movie in reversed(movies):
        for video in database:
            if video[0] == os.path.split(movie)[1]:  # TODO dorobić trochę bardziej rozbudowane sprawdzanie niż nazwa
                # os.path.samefile() ?
                movies.remove(movie)  # Przemyśleć tego remove tak żeby pomniejszać listę a żeby nie psuło
                break
        else:
            found.append(movie)
    return found


def run_fast_scandir(dir, ext, recursive=False):
    subfolders, files = [], []
    for f in os.scandir(dir):
        if f.is_dir():
            subfolders.append(f.path)
        if f.is_file():
            if os.path.splitext(f.name)[1].lower() in ext:
                files.append(f.path)
    if recursive:
        for dir in list(subfolders):
            sf, f = run_fast_scandir(dir, ext, recursive)
            subfolders.extend(sf)
            files.extend(f)

    return subfolders, files


def delete_file(fname):
    print("Delete file ", fname)
    files_used(fname, "Delete File Thread")
    for i in range(3):
        try:
            print("Nastąpi usuniecie pliku " + str(fname) + " próba " + str(i+1))
            os.remove(fname)
            print("Plik został usunięty")
            break
        except PermissionError as err:
            print(err)
        except FileNotFoundError as err2:
            print(err2)
        except:
            print("Unexpected error:", exc_info()[0])
            raise
        sleep(5)


def files_used(file, where):
    for proc in psutil.process_iter():
        try:
            if proc.name() == 'python.exe':
                for nt in proc.open_files():
                    if os.path.normcase(nt.path) == os.path.normcase(file):
                        print("{}, {}, {}".format(proc.name(), proc.pid, where))
                        print("\t", nt.path)
        except Exception as e:
            print("files_used exception: ", e)


def add_spaces_to_text(spaces, text):
    new_str = ""
    for i in range(spaces):
        new_str += " "
    return new_str+text


def file_with_ext_from_path(vid_path):
    return os.path.basename(vid_path)


def file_from_path_without_ext(vid_path):
    path_with_ext = file_with_ext_from_path(vid_path)
    # TODO Delete ext
    return path_with_ext


def create_video_dir_proj_path(path):  # TODO W ogóle zastanowić się nad miejscami wywoływania tej funkcji
    file_name = file_with_ext_from_path(path)
    project_dir = project_dir_read()
    vid_dir = QDir(project_dir)
    # TODO może tworzyć potrzebne foldery an starcie od razu
    vid_dir.mkdir('Videos')
    vid_dir.mkdir('Avatars')  # TODO To jest wywoływane w wielu miejscach, czy to nie jest potencjalny bottleneck
    vid_dir.cd("Videos")
    vid_dir.mkdir(file_name[0].upper())
    vid_dir.cd(file_name[0].upper())
    return vid_dir.path() + "/" + file_name


def is_path_in_proj_dir(path):  # TODO to be tested
    path_object = Path(path)
    dir_list = path_object.parts
    proj_dir = project_dir_read()
    proj_dir_obj = Path(proj_dir)
    proj_dir_list = proj_dir_obj.parts
    is_good_path = True
    print(dir_list)
    print(proj_dir_list)
    for i in range(len(proj_dir_list)):
        is_good_path &= dir_list[i] == proj_dir_list[i]
    return is_good_path


def is_proper_proj_video_dir_path(path):
    proper_path = create_video_dir_proj_path(path)
    return proper_path == path


def move_file(src_path, dst_path):
    shutil.move(src_path, dst_path)
