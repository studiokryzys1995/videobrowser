from Models.SearchActressModel import SearchActress


class ChoseActress(SearchActress):
    def __init__(self):
        super(ChoseActress, self).__init__()
        self.ui.list_search.doubleClicked.connect(self.item_dclicked)

    def item_dclicked(self, index):
        for act in self.search_result:
            if act.name == index.data():
                self.selected.append(act)
        self.close()

    def return_result(self):
        return self.selected
