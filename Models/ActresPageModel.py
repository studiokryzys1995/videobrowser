from GUIs.ActressPage import Ui_Dialog
from Database.Database import VideoDatabase
from PyQt5.QtWidgets import QDialog, QFileDialog
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, QDate, QStringListModel
from Players.VideoPlayer import VideoPlayer
from Misc.ConfigFunctions import project_dir_read


class ActressPage(QDialog):
    def __init__(self, act):
        super(ActressPage, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.db = VideoDatabase()
        self.ui.list_video.doubleClicked.connect(self.item_dclicked)
        self.actress = act
        self.videos = []
        self.selected = None
        self.fill_data()
        self.ui.btn_avatar.clicked.connect(self.change_avatar)

    def fill_data(self):
        self.ui.edit_name.setText(self.actress.name)
        self.ui.label_avatar.setPixmap(QPixmap(self.actress.avatar).scaled(
            self.ui.label_avatar.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        self.ui.edit_origin.setText(self.actress.origins)
        date = QDate.fromString(self.actress.date)
        self.ui.edit_date.setDate(date)
        current_date = QDate.currentDate()
        current_age = current_date.year() - date.year()
        if date.month() > current_date.month() or date.month() == current_date.month() and date.day() > current_date.day():
            current_age -= 1
        self.ui.label_age.setText(str(current_age))
        self.video_list()

    def update_actress_args(self):
        self.actress.name = self.ui.edit_name.text()
        self.actress.date = self.ui.edit_date.date().toString()
        self.actress.origins = self.ui.edit_origin.text()

    def change_avatar(self):
        # TODO Powtorzenie kodu
        dialog = QFileDialog()
        project_dir = project_dir_read()
        # TODO Dodaj folder Avatars jak nie istnieje
        path, opt = dialog.getOpenFileName(self, "Chose avatar", project_dir + "/Avatars")
        if path:
            photo = QPixmap(path).scaled(self.ui.label_avatar.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.ui.label_avatar.setPixmap(photo)
            self.actress.avatar = path

    def video_list(self):
        self.videos = self.db.select_actress_videos(self.actress.act_id)
        model = QStringListModel(self)
        titles = []
        for vid in self.videos:
            titles.append(vid.title)
        titles.sort()
        model.setStringList(titles)
        self.ui.list_video.setModel(model)

    def item_dclicked(self, index):
        for vid in self.videos:
            if vid.title == index.data():
                self.selected = vid
        player = VideoPlayer(self.selected)
        player.exec()

    def closeEvent(self, event):
        self.update_actress_args()
        self.db.update_actress(self.actress)


