import sqlite3 as db
from Misc.Singleton import Singleton
from Database.Video import Video
from Database.Actress import Actress
import os


class VideoDatabase(object, metaclass=Singleton):
    def __init__(self):
        self.conn = None

    def get_connection(self):
        return self.conn

    def create_connection(self, db_filename):
        try:
            self.conn = db.connect(db_filename)
            return True
        except db.Error as e:
            print("VideoDatabase: ", e)
            return False

    def is_connection(self):
        if self.conn:
            return True
        else:
            return False

    def close_connection(self):
        if self.conn:
            self.conn.close()
            self.conn = None

    def create_tables(self):
        with open('Database/vidbro.sql', 'r') as f:
            qry = f.read()
        if self.conn:
            try:
                c = self.conn.cursor()
                c.executescript(qry)
                self.conn.commit()
                c.close()
            except db.Error as e:
                print("VideoDatabase: ", e)
        else:
            print("VideoDatabase: ", "No DB connection")

    def insert(self, sql, arg):
        if self.conn:
            try:
                c = self.conn.cursor()
                c.execute(sql, arg)
                self.conn.commit()
                last_row = c.lastrowid
                c.close()
                return last_row
            except db.DatabaseError as e:
                print("VideoDatabase: ", e)
        else:
            print("Brak połaczenia z DB")

    def insert_video(self, video):
        return self.insert('''INSERT INTO VIDEO(title, producer, path, thumb, duration, resolution_x, resolution_y, 
                            gens_id, mark, favourite, last_view, view_count, compression, archive) 
                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);''', video.atr_to_tuple())

    def insert_actress_video(self, vid_id, act_id):
        self.insert('''INSERT INTO ACTRESS_VIDEOS(vid_id, act_id) 
                             VALUES (?,?);''', (vid_id, act_id))

    def insert_actress(self, name, date, origins, path):
        self.insert('''INSERT INTO ACTRESS(name,date,origins,avatar) 
                             VALUES (?,?,?,?);''', (name, date, origins, path))

    def update(self, sql, arg):
        if self.conn:
            try:
                c = self.conn.cursor()
                c.execute(sql, arg)
                self.conn.commit()
                c.close()
            except db.DatabaseError as e:
                print("VideoDatabase: ", e)
        else:
            print("Brak połaczenia z DB")

    def update_video(self, video):
        self.update("UPDATE VIDEO SET title=?, producer=?, path=?, thumb=?, duration=?, resolution_x=?, resolution_y=?,"
                    "gens_id=?, mark=?, favourite=?, last_view=?, view_count=?, compression=?, archive=? "
                    "WHERE vid_id=?", video.atr_to_tuple()+(video.vid_id,))

    def update_actress(self, act):
        self.update("UPDATE ACTRESS SET name=?, date=?, origins=?, avatar=? WHERE act_id=?", act.atr_to_tuple()+(act.act_id,))

    def select(self, select, arg=None):
        if self.conn:
            results = None
            try:
                c = self.conn.cursor()
                if arg:
                    c.execute(select, arg)
                else:
                    c.execute(select)
                results = c.fetchall()
                c.close()
            except db.DatabaseError as e:
                print("VideoDatabase: ", e)
            return results
        else:
            print("Brak połaczenia z DB")

    def select_video(self, select, arg=None):
        results = self.select(select, arg)
        vids = []
        for res in results:
            v = Video()
            v.fill_from_data(res)
            vids.append(v)
        return vids

    def select_video_by_title(self, phrase):
        return self.select_video("SELECT * FROM VIDEO WHERE title LIKE ?;", ('%'+phrase+'%',))

    def select_video_by_actress(self, phrase):
        return self.select_video("SELECT * FROM VIDEO INNER JOIN ACTRESS_VIDEOS AV on VIDEO.vid_id = AV.vid_id "
                                 "INNER JOIN ACTRESS AC on AV.act_id = AC.act_id "
                                 "WHERE AC.name LIKE ?;", ('%'+phrase+'%',))

    def select_all_video(self):
        return self.select_video("SELECT * FROM VIDEO;")

    def is_video_in_base(self, video_path):
        video_filename_with_path = os.path.splitext(os.path.abspath(video_path))[0]
        video_filename_striped = video_filename_with_path.split(os.sep)[-1]
        db_paths = self.select("SELECT path FROM VIDEO WHERE path LIKE ?", ('%'+video_filename_striped+'%',))
        if db_paths:
            for db_path in db_paths:
                db_filename_with_path = os.path.splitext(os.path.abspath(db_path[0]))[0]
                db_filename_striped = db_filename_with_path.split(os.sep)[-1]
                if video_filename_striped == db_filename_striped:
                    return True
        return False

    def select_actress(self, select, arg=None):
        results = self.select(select, arg)
        act = []
        for res in results:
            a = Actress()
            a.fill_from_data(res)
            act.append(a)
        return act

    def select_all_actress(self):
        return self.select_actress("SELECT * FROM ACTRESS;")

    def select_actress_by_name(self, name):
        return self.select_actress("SELECT * FROM ACTRESS WHERE name LIKE ?;", ('%'+name+'%',))

    def is_actress_in_base(self, name):
        return self.select("SELECT name FROM ACTRESS WHERE name = ?", (name,))

    def select_actress_videos(self, a_id):
        return self.select_video("SELECT * FROM VIDEO "
                                 "INNER JOIN ACTRESS_VIDEOS AV on VIDEO.vid_id = AV.vid_id "
                                 "WHERE AV.act_id = ?", (a_id,))

    def select_video_actresses(self, v_id):
        return self.select_actress("SELECT * FROM ACTRESS "
                                   "INNER JOIN ACTRESS_VIDEOS AV on ACTRESS.act_id = AV.act_id "
                                   "WHERE AV.vid_id = ?", (v_id,))

    def delete(self, sql, arg):
        if self.conn:
            try:
                c = self.conn.cursor()
                c.execute(sql, arg)
                self.conn.commit()
                c.close()
            except db.DatabaseError as e:
                print("VideoDatabase: ", e)
        else:
            print("Brak połaczenia z DB")

    def delete_video(self, video):
        self.delete("DELETE FROM VIDEO WHERE vid_id = ?", (video.vid_id,))

    # Not necessary after delete_video because of ON DELETE CASCADE
    def delete_actress_video(self, vid_id, act_id):
        self.delete("DELETE FROM ACTRESS_VIDEOS WHERE vid_id = ? AND act_id = ?", (vid_id, act_id))
