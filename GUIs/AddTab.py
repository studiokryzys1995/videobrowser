# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'P:\ProgramingProjects\PythonProjects\VideoBrowser\GUIs\AddTab.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AddMedia(object):
    def setupUi(self, AddMedia):
        AddMedia.setObjectName("AddMedia")
        AddMedia.resize(500, 732)
        self.tabWidget = QtWidgets.QTabWidget(AddMedia)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 501, 731))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tabWidget.setObjectName("tabWidget")

        self.retranslateUi(AddMedia)
        self.tabWidget.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(AddMedia)

    def retranslateUi(self, AddMedia):
        _translate = QtCore.QCoreApplication.translate
        AddMedia.setWindowTitle(_translate("AddMedia", "Add Media"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AddMedia = QtWidgets.QDialog()
    ui = Ui_AddMedia()
    ui.setupUi(AddMedia)
    AddMedia.show()
    sys.exit(app.exec_())
