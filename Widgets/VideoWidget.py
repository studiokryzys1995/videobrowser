from PyQt5.QtWidgets import QWidget
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtCore import QEvent, Qt, QTimer
from Widgets.MovableSlider import MovableSlider
from PyQt5.QtGui import QPalette


class VideoWidget(QWidget):
    def __init__(self, context):
        super(VideoWidget, self).__init__(context)
        self.video_widget = QVideoWidget(self)
        self.slider = MovableSlider(Qt.Horizontal, self)
        self.slider.hide()
        self.slider.move(100, 100)
        self.slider.setRange(0, 100)

        self.installEventFilter(self)
        self.setMouseTracking(True)
        self.timer_mouse_dc = QTimer()
        self.timer_mouse_dc.setSingleShot(True)
        self.timer_mouse_dc.timeout.connect(context.play)
        self.timer_mouse_hide = QTimer()
        self.timer_mouse_hide.setSingleShot(True)
        self.timer_mouse_hide.timeout.connect(self.hide_cursor)

    def changeEvent(self, event):
        if event.type() == QEvent.WindowStateChange:
            self.slider.setVisible(self.windowState() == Qt.WindowFullScreen)
        super().changeEvent(event)

    def resizeEvent(self, event):
        self.video_widget.resize(self.size())
        event.accept()

    def hide_cursor(self):
        self.setCursor(Qt.BlankCursor)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.MouseButtonDblClick:
            if self.isFullScreen():
                self.showNormal()
                self.video_widget.setFullScreen(False)
                self.setCursor(Qt.ArrowCursor)
                self.timer_mouse_hide.stop()
            else:
                self.video_widget.setFullScreen(True)
                self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
                self.setFocus()
                self.showFullScreen()
            self.timer_mouse_dc.stop()
            return True
        if event.type() == QEvent.MouseButtonPress:
            if self.timer_mouse_dc.isActive():
                pass
                # Przycisk zostal wcześniej klikniety, mniej niż x ms temu
            else:
                self.timer_mouse_dc.start(250)
            return True
        if event.type() == QEvent.MouseMove:
            if self.isFullScreen():
                self.setCursor(Qt.ArrowCursor)
                self.timer_mouse_hide.start(2000)
            return True
        return False