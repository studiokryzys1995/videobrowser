from PyQt5.QtWidgets import QDialog, QFileDialog, QPushButton
from GUIs.BatchMoveVideos import Ui_Dialog
from Widgets.CheckBoxVideoWidget import CheckBoxVideoWidget
from Widgets.ScrollCheckBoxArea import ScrollCheckBoxArea
from Database.Database import VideoDatabase
from Misc.functions import move_file, file_with_ext_from_path, print_warning, create_video_dir_proj_path, is_proper_proj_video_dir_path
import os


class BatchMoveModel(QDialog):
    def __init__(self):
        super(BatchMoveModel, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.check_box.stateChanged.connect(self.move_to_proj_selected)
        self.db = VideoDatabase()
        self.check_box_area = ScrollCheckBoxArea(self)
        self.ui.verticalLayout.addWidget(self.check_box_area)
        self.btn_confirm = QPushButton()
        self.btn_confirm.setText("Wybierz")
        self.ui.verticalLayout.addWidget(self.btn_confirm)
        self.videos = None
        self.init_movies()
        self.ui.btn_search.clicked.connect(self.filter)
        self.ui.btn_chose.clicked.connect(self.open_dir_dialog)
        self.btn_confirm.clicked.connect(self.move_videos)
        self.new_dir = None
        self.is_checked = False
        self.generate_path = self.generate_path_custom

    def draw_list(self):
        self.check_box_area.clean_entries()
        for video in self.videos:
            new_widget = CheckBoxVideoWidget(video)
            self.check_box_area.add_entry(new_widget)

    def init_movies(self):
        self.videos = self.db.select_all_video()
        self.draw_list()

    def filter(self):
        search = self.ui.edit_search.text()
        self.videos = self.db.select_video_by_title(search)
        self.draw_list()

    def move_to_proj_selected(self):
        self.is_checked = self.ui.check_box.isChecked()
        self.ui.btn_chose.setEnabled(not self.is_checked)
        if self.is_checked:
            self.generate_path = self.generate_path_proj
        else:
            self.generate_path = self.generate_path_custom

    def generate_path_custom(self, video):
        file_name = file_with_ext_from_path(video.path)
        return os.path.normpath(os.path.join(self.new_dir, file_name))

    def generate_path_proj(self, video):
        if is_proper_proj_video_dir_path(video.path):
            print("Video znajduje się już w projekcie, pomijam...")
            return None
        return create_video_dir_proj_path(video.path)

    def open_dir_dialog(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setOption(QFileDialog.ShowDirsOnly)
        self.new_dir = dialog.getExistingDirectory(self, "Chose Directory", "K://Sekretny Folder")
        self.ui.label_path.setText(self.new_dir)

    def move_videos(self):
        if self.new_dir is None and not self.is_checked:
            print_warning("No new dir chosen")
            return

        videos_list = self.check_box_area.get_checked_widgets()
        if len(videos_list) > 0:
            for video in videos_list:
                new_path = self.generate_path(video)
                if new_path is None:
                    continue
                move_file(video.path, new_path)
                video.path = new_path
                self.db.update_video(video)

        # TODO dodanie paska progresu
        self.close()
