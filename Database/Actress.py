from Database.Table import Table


class Actress(object):
    def __init__(self):
        self.act_id = None
        self.name = None
        self.date = None
        self.origins = None
        self.avatar = None

    def fill_from_data(self, data):
        self.act_id = data[0]
        self.name = data[1]
        self.date = data[2]
        self.origins = data[3]
        self.avatar = data[4]

    def atr_to_tuple(self):
        return self.name, self.date, self.origins, self.avatar
