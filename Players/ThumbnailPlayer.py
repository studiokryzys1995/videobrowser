from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QPushButton, QSpacerItem, \
    QSizePolicy
from PyQt5.QtMultimedia import QMediaMetaData
from Widgets.ThumbnailWidget import VideoWidget
from Players.AbstractPlayer import Player


class ThumbBrowser(Player):
    def __init__(self):
        super(ThumbBrowser, self).__init__()
        self.setObjectName("Player")
        self.resize(1024, 800)
        self.setWindowTitle("Thumbnail browser")
        self.setFocusPolicy(Qt.StrongFocus)
        self.setFocus()
        self.setWindowModality(Qt.ApplicationModal)
        self.videoWidget = VideoWidget(self)
        self.image = None

        self.accept_button = QPushButton("Wybierz")
        self.accept_button.setEnabled(False)
        self.accept_button.clicked.connect(self.chose)

        self.btn_next_more = QPushButton("+5s")
        self.btn_next_more.clicked.connect(self.time_next_more)
        self.btn_next = QPushButton("+1s")
        self.btn_next.clicked.connect(self.time_next)
        self.btn_prev = QPushButton("-1s")
        self.btn_prev.clicked.connect(self.time_prev)
        self.btn_prev_more = QPushButton("-5s")
        self.btn_prev_more.clicked.connect(self.time_prev_more)

        buttonsLayout = QHBoxLayout()
        buttonsLayout.addItem(QSpacerItem(QSpacerItem(50, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)))
        buttonsLayout.addWidget(self.btn_prev_more)
        buttonsLayout.addWidget(self.btn_prev)
        buttonsLayout.addWidget(self.btn_next)
        buttonsLayout.addWidget(self.btn_next_more)
        buttonsLayout.addItem(QSpacerItem(QSpacerItem(50, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)))

        controlsLayout = QHBoxLayout()
        controlsLayout.addItem(QSpacerItem(QSpacerItem(50, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)))
        controlsLayout.addWidget(self.accept_button)
        controlsLayout.addItem(QSpacerItem(QSpacerItem(50, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)))

        self.set_video_surface(self.videoWidget.surface)
        self.layout.addWidget(self.videoWidget)
        self.layout.addLayout(buttonsLayout)
        self.init_layout()
        self.layout.addLayout(controlsLayout)

    def set_video(self, url):
        super().set_video(url)
        self.accept_button.setEnabled(True)

    def get_vid_info(self):
        dur = self.player.duration()
        res = self.player.metaData(QMediaMetaData.Resolution)
        codec = self.player.metaData(QMediaMetaData.VideoCodec)
        if res:
            x = res.width()
            y = res.height()
        else:
            x, y = 0, 0
        return [dur, x, y, codec]

    def default_thumb(self):
        pass
        # Oczekiwanie na unifikacje playerów
        #self.setPosition(0)
        #print(self.player.availableMetaData())
        # if QMediaMetaData.PosterImage in self.player.availableMetaData():
        #     self.player.metaData(QMediaMetaData.PosterImage)

    def chose(self):
        self.player.stop()
        self.close()

    def return_image(self):
        return self.videoWidget.return_thumbnail()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Right:
            pos = self.player.position()
            if pos < self.player.duration():
                self.setPosition(pos + 1000)
        elif event.key() == Qt.Key_Left:
            pos = self.player.position()
            if pos > 0:
                self.setPosition(pos - 1000)
        super().keyPressEvent(event)

    def time_next_more(self):
        pos = self.player.position()
        if pos < self.player.duration():
            self.setPosition(pos + 5000)

    def time_prev_more(self):
        pos = self.player.position()
        if pos > 0:
            self.setPosition(pos - 5000)

    def time_prev(self):
        pos = self.player.position()
        if pos > 0:
            self.setPosition(pos - 1000)

    def time_next(self):
        pos = self.player.position()
        if pos < self.player.duration():
            self.setPosition(pos + 1000)

