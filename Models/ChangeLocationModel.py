from PyQt5.QtWidgets import QDialog, QFileDialog
from PyQt5.QtCore import QDir
from GUIs.ChangeLocation import Ui_Dialog
from Database.Database import VideoDatabase
from Misc.ConfigFunctions import project_dir_read
from Misc.functions import print_warning, file_with_ext_from_path, move_file
import os


class ChangeLocation(QDialog):
    def __init__(self, video):
        super(ChangeLocation, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.db = VideoDatabase()
        self.video = video
        self.project_dir = project_dir_read()

        self.ui.btn_ok.clicked.connect(self.yes_clicked)
        self.ui.btn_cancel.clicked.connect(self.no_clicked)
        self.ui.btn_location.clicked.connect(self.browse)
        self.ui.btn_default_location.clicked.connect(self.default_dir)

        self.ui.label_location_old.setText(video.path)

        self.new_dir = ""

    def yes_clicked(self):
        if self.new_dir != "":
            new_path = self.new_dir + "/" + file_with_ext_from_path(self.video.path)
            if new_path != self.video.path:
                move_file(self.video.path, new_path)
                self.video.path = new_path
                self.db.update_video(self.video)
                self.close()
            else:
                print_warning("Plik znajduje się już w podanej ścieżce")
        else:
            print_warning("Nie podano nowego katalogu")

    def browse(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setOption(QFileDialog.ShowDirsOnly)
        dir_path = dialog.getExistingDirectory(self, "Chose Directory", self.project_dir)
        if dir_path:
            self.new_dir = dir_path
            self.ui.label_location_new.setText(dir_path)

    def default_dir(self):
        vid_dir = QDir(self.project_dir)
        split_video_name = file_with_ext_from_path(self.video.path)
        vid_dir.mkdir('Videos')
        vid_dir.cd("Videos")
        vid_dir.mkdir(split_video_name[0].upper())
        vid_dir.cd(split_video_name[0].upper())
        self.new_dir = vid_dir.path()
        self.ui.label_location_new.setText(self.new_dir)

    def no_clicked(self):
        self.close()
