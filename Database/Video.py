# from Database.Table import Table
# from Database.Database import VideoDatabase
from PyQt5.QtCore import QSize


class Video(object):
    def __init__(self):
        self.vid_id = None
        self.title = None
        self.producer = None
        self.path = None
        self.thumb = None
        self.duration = None
        self.resolution_x = None
        self.resolution_y = None
        self.gens_id = None
        self.mark = None
        self.favourite = None
        self.last_view = None
        self.view_count = None
        self.compression = None
        self.archive = None

    def fill_from_data(self, data):
        self.vid_id = data[0]
        self.title = data[1]
        self.producer = data[2]
        self.path = data[3]
        self.thumb = data[4]
        self.duration = data[5]
        self.resolution_x = data[6]
        self.resolution_y = data[7]
        self.gens_id = data[8]
        self.mark = data[9]
        self.favourite = data[10]
        self.last_view = data[11]
        self.view_count = data[12]
        self.compression = data[13]
        self.archive = data[14]

    def get_qsize(self):
        return QSize(self.resolution_x, self.resolution_y)

    def atr_to_tuple(self):
        return (self.title, self.producer, self.path, self.thumb, self.duration, self.resolution_x,
                self.resolution_y, self.gens_id, self.mark, self.favourite, self.last_view, self.view_count,
                self.compression, self.archive)


