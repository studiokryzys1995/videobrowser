from PyQt5.QtWidgets import QLabel, QSizePolicy
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QPalette, QPixmap


class AspectWidget(QLabel):

    def __init__(self, pix_path):
        super(AspectWidget, self).__init__()
        pix = QPixmap(pix_path)
        self.unchanged_pixmap = pix
        self.setPixmap(pix)
        self.ratio = 1

        palette = self.palette()
        palette.setColor(QPalette.Background, Qt.black)
        self.setPalette(palette)
        self.setAutoFillBackground(True)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Preferred,
                                       QSizePolicy.Ignored))
        self.newHeight = 0

    def set_ratio(self, ratio):
        self.ratio = ratio

    def hasHeightForWidth(self):
        return True

    def heightForWidth(self, width):
        height = width * self.ratio
        return height

    def sizeHint(self):
        self.newHeight = int(self.heightForWidth(self.width()))
        return QSize(self.width(), self.newHeight)

    def resizeEvent(self, event):
        event.accept()
        # print("Outside ", self.height(), " ", self.newHeight)
        if self.unchanged_pixmap:
            pix = self.unchanged_pixmap.scaled(self.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.setPixmap(pix)
        # if self.newHeight != self.height():
        #     print("Inside ", self.height(), " ", self.newHeight)
        #     #self.updateGeometry()
        #     self.resize(self.sizeHint())
