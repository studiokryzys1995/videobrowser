from GUIs.AddTab import Ui_AddMedia
from Models.AddVideoModel import AddVideo
from Models.AddActressModel import AddActress
from PyQt5.QtWidgets import QDialog, QMessageBox


class AddTab(QDialog):
    def __init__(self, db, dir_path):
        super().__init__()
        ui = Ui_AddMedia()
        ui.setupUi(self)
        ui.tabWidget.addTab(AddVideo(db, dir_path), "AddVideo")
        ui.tabWidget.addTab(AddActress(db), "AddActress")