# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'P:\ProgramingProjects\PythonProjects\VideoBrowser\GUIs\SearchFolders.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(539, 570)
        self.btn_del = QtWidgets.QPushButton(Dialog)
        self.btn_del.setGeometry(QtCore.QRect(70, 510, 111, 23))
        self.btn_del.setObjectName("btn_del")
        self.btn_add = QtWidgets.QPushButton(Dialog)
        self.btn_add.setGeometry(QtCore.QRect(390, 510, 75, 23))
        self.btn_add.setObjectName("btn_add")
        self.check_recursion = QtWidgets.QCheckBox(Dialog)
        self.check_recursion.setGeometry(QtCore.QRect(400, 30, 81, 17))
        self.check_recursion.setObjectName("check_recursion")
        self.btn_open = QtWidgets.QPushButton(Dialog)
        self.btn_open.setGeometry(QtCore.QRect(40, 30, 91, 23))
        self.btn_open.setObjectName("btn_open")
        self.label_path = QtWidgets.QLabel(Dialog)
        self.label_path.setGeometry(QtCore.QRect(150, 30, 231, 21))
        self.label_path.setObjectName("label_path")
        self.list_files = QtWidgets.QListView(Dialog)
        self.list_files.setGeometry(QtCore.QRect(50, 90, 441, 341))
        self.list_files.setObjectName("list_files")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.btn_del.setText(_translate("Dialog", "Usuń"))
        self.btn_add.setText(_translate("Dialog", "Dodaj"))
        self.check_recursion.setText(_translate("Dialog", "Rekursywnie"))
        self.btn_open.setText(_translate("Dialog", "Otwórz folder"))
        self.label_path.setText(_translate("Dialog", "Ścieżka"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
