from PyQt5.QtWidgets import QWidget, QLabel, QMenu, QAction, QSizePolicy, QVBoxLayout
from Players.VideoPlayer import VideoPlayer
from Models.VideoPageModel import VideoPage
from Models.VideoDeleteModel import DeleteVideo
from Misc.functions import duration_to_sting
from PyQt5.QtCore import Qt
from Widgets.AspectWidget import AspectWidget
from os import startfile, path


class SmallWidget(QWidget):
    def __init__(self, vid):
        super().__init__()

        self.video = vid

        self.thumb = AspectWidget(vid.thumb)
        # TODO przerobić na settings
        self.thumb.set_ratio(9/16)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.show_context_menu)

        self.label = QLabel(vid.title)
        self.label.setSizePolicy(QSizePolicy(QSizePolicy.Minimum,
                                             QSizePolicy.Fixed))
        self.setToolTip("Tytuł: {} \n Długość: {}".format(vid.title, duration_to_sting(vid.duration)))
        lay = QVBoxLayout()
        lay.addWidget(self.thumb)
        lay.addWidget(self.label)
        self.setLayout(lay)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Ignored,
                                       QSizePolicy.Ignored))

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            player = VideoPlayer(self.video)
            player.exec()
            event.accept()

    def show_context_menu(self, pos):
        menu = QMenu("Context menu", self)
        action1 = QAction("Video page", self)
        action1.triggered.connect(self.show_video_page)
        action2 = QAction("Delete video", self)
        action2.triggered.connect(self.show_delete_page)
        action3 = QAction("Open in explorer", self)
        action3.triggered.connect(self.open_in_explorer)
        action4 = QAction("Open in player", self)
        action4.triggered.connect(self.open_in_player)
        menu.addAction(action1)
        menu.addAction(action2)
        menu.addAction(action3)
        menu.addAction(action4)
        menu.exec(self.mapToGlobal(pos))

    def show_video_page(self):
        vid_page = VideoPage(self.video)
        vid_page.exec()

    def show_delete_page(self):
        del_page = DeleteVideo(self.video)
        del_page.exec()

    def open_in_player(self):
        startfile(self.video.path)

    def open_in_explorer(self):
        dir_from_path = path.dirname(self.video.path)
        startfile(dir_from_path)
